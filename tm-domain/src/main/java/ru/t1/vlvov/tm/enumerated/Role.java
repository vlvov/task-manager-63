package ru.t1.vlvov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @NotNull
    private final String displayName;

    Role(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Role toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final Role role : values()) {
            if (value.equals(role.getDisplayName())) return role;
        }
        return null;
    }

}
