package ru.t1.vlvov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldNotFoundException {

    public ProjectIdEmptyException() {
        super("Error! Project Id is empty...");
    }

}
