package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.vlvov.tm.module.Project;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ProjectRepository {

    private Map<String, Project> projects = new HashMap<>();

    public final static ProjectRepository INSTANCE = new ProjectRepository();

    private ProjectRepository() {

    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    {
        add(new Project("TEST"));
        add(new Project("BETA"));
        add(new Project("MEGA"));
    }

    public void add(final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(final Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

    public void create() {
        add(new Project("New Project" + System.currentTimeMillis()));
    }

}
