package ru.t1.vlvov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.dto.request.DataBackupLoadRequest;
import ru.t1.vlvov.tm.event.ConsoleEvent;

@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load backup data from base 64 file.";

    @NotNull
    public static final String NAME = "data-load-backup";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBackupLoadListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @NotNull DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        domainEndpoint.loadDataBackup(request);
    }

}
