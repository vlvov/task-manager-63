package ru.t1.vlvov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.event.ConsoleEvent;
import ru.t1.vlvov.tm.listener.AbstractListener;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class FileScanner {

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final List<String> commands = new ArrayList();

    @NotNull
    private final File folder = new File(".\\");

    public void process() {
        for (final File file : folder.listFiles()) {
            if (file.isDirectory()) continue;
            final String fileName = file.getName();
            if (commands.contains(fileName)) {
                file.delete();
                bootstrap.getPublisher().publishEvent(new ConsoleEvent(fileName));
            }
        }
    }

    public void stop() {
        es.shutdown();
    }

    public void start() {
        Iterable<AbstractListener> commands = Arrays.asList(bootstrap.getAbstractListeners());
        commands.forEach(e -> this.commands.add(e.getName()));
        es.scheduleAtFixedRate(this::process, 0, 5, TimeUnit.SECONDS);
    }

}
