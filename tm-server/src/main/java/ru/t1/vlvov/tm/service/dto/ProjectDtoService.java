package ru.t1.vlvov.tm.service.dto;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.vlvov.tm.api.service.dto.IProjectDtoService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.exception.entity.EntityNotFoundException;
import ru.t1.vlvov.tm.exception.field.IdEmptyException;
import ru.t1.vlvov.tm.exception.field.NameEmptyException;
import ru.t1.vlvov.tm.repository.dto.ProjectDtoRepository;

import java.util.Collection;
import java.util.List;

@Service
@Getter
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository projectRepositoryDTO;

    @Override
    @NotNull
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO model = new ProjectDTO();
        model.setName(name);
        model.setUserId(userId);
        add(userId, model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name, @NotNull final String description) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull final ProjectDTO model = new ProjectDTO();
        model.setName(name);
        model.setUserId(userId);
        model.setDescription(description);
        add(userId, model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public ProjectDTO changeProjectStatusById(@Nullable String userId, @Nullable String id, @NotNull Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setStatus(status);
        projectRepositoryDTO.save(model);
        return model;
    }

    @Override
    @NotNull
    @Transactional
    public ProjectDTO updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @NotNull String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        model.setName(name);
        model.setDescription(description);
        projectRepositoryDTO.save(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() {
        projectRepositoryDTO.deleteAll();
    }

    @Override
    @Transactional
    public void add(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void set(@NotNull Collection<ProjectDTO> collection) {
        clear();
        projectRepositoryDTO.saveAll(collection);
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll() {
        return projectRepositoryDTO.findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        projectRepositoryDTO.delete(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectRepositoryDTO.deleteById(id);
    }

    @Override
    @Transactional
    public boolean existsById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepositoryDTO.existsById(id);
    }

    @Override
    public @Nullable ProjectDTO findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectRepositoryDTO.findFirstById(id);
    }

    @Override
    @Transactional
    public void add(@Nullable String userId, @Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        model.setUserId(userId);
        projectRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void update(@Nullable String userId, @Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        model.setUserId(userId);
        projectRepositoryDTO.save(model);
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        projectRepositoryDTO.deleteAllByUserId(userId);
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return projectRepositoryDTO.findByUserId(userId);
    }

    @Override
    public @Nullable ProjectDTO findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return projectRepositoryDTO.findFirstByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void remove(@Nullable String userId, @Nullable ProjectDTO model) {
        if (model == null) throw new EntityNotFoundException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        projectRepositoryDTO.deleteByUserIdAndId(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        ProjectDTO model = findOneById(userId, id);
        remove(userId, model);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Override
    @Nullable
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable CustomSort sort) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        if (sort == null) return findAll(userId);
        final Sort sortable = Sort.by(Sort.Direction.ASC, CustomSort.getOrderByField(sort));
        return projectRepositoryDTO.findByUserId(userId, sortable);
    }

}
