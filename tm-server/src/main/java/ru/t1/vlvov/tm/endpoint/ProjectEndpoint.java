package ru.t1.vlvov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.service.dto.IProjectDtoService;
import ru.t1.vlvov.tm.api.service.model.IProjectService;
import ru.t1.vlvov.tm.dto.model.ProjectDTO;
import ru.t1.vlvov.tm.dto.model.SessionDTO;
import ru.t1.vlvov.tm.dto.request.*;
import ru.t1.vlvov.tm.dto.response.*;
import ru.t1.vlvov.tm.enumerated.CustomSort;
import ru.t1.vlvov.tm.enumerated.Status;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDtoService projectServiceDTO;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Override
    @WebMethod
    public ProjectAddResponse addProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectAddRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        projectServiceDTO.add(userId, request.getProject());
        return new ProjectAddResponse(request.getProject());
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String statusValue = request.getStatus();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull ProjectDTO project = projectServiceDTO.changeProjectStatusById(userId, projectId, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectClearRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        projectService.clear(userId);
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull ProjectDTO project = projectServiceDTO.create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final CustomSort sort = request.getSort();
        @Nullable final List<ProjectDTO> projects = projectServiceDTO.findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable ProjectDTO project = projectServiceDTO.findOneById(userId, projectId);
        projectService.removeById(userId, projectId);
        return new ProjectRemoveByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getProjectId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull ProjectDTO project = projectServiceDTO.updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectGetByIdResponse getById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectGetByIdRequest request
    ) {
        @NotNull SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @NotNull ProjectDTO project = projectServiceDTO.findOneById(userId, projectId);
        return new ProjectGetByIdResponse(project);
    }

}
